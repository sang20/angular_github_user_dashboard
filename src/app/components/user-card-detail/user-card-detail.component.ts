import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { UserData } from './user-card-detail.model';

@Component({
  selector: 'app-user-card-detail',
  templateUrl: './user-card-detail.component.html',
  styleUrls: ['./user-card-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UserCardDetailComponent implements OnInit {
  @Input('data') data: UserData;
  @Input('pageView') pageView: string;

  constructor() {}

  ngOnInit() {
  }

}

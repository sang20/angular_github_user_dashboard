
export interface UserData {
    avatar_url?: string;
    name?: string;
    login?: string;
    company?: string;
    followers?: string;
    public_repos?: string;
    html_url?: string;
  }


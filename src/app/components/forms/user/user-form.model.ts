
export interface UserPayloadDefinition {
    name: string;
    user_name: string;
    email_address: string;
    company?: string;
  }

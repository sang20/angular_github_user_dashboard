import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from 'src/app/services/utils/utils.service';
import { UserPayloadDefinition } from './user-form.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  payload: UserPayloadDefinition;
  isFormSubmitted: boolean = false;
  formData: string;
  constructor(
    private fb: FormBuilder,
    private utils: UtilityService ) {}

  // user form form builder definitions
  userForm = this.fb.group({
    name: ['', Validators.required],
    user_name: ['', Validators.required],
    email_address: ['', [Validators.required, Validators.email, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
    company: ['']
  });

  ngOnInit(): void {
  }

  cancel(url: string = ''): void {
    this.utils.navigateToUrl(url);
  }

  submitForm(): void {
    this.isFormSubmitted = true;
    // set payload
    this.payload = {
      name: this.userForm.controls.name.value,
      user_name: this.userForm.controls.user_name.value,
      email_address: this.userForm.controls.email_address.value,
      company: this.userForm.controls.company.value,
    };
    this.formData = JSON.stringify(this.payload);
    // check if form is valid
    // make a POST request and pass the payload API
  }

}

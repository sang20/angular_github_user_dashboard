import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  // api url
  apiUrl: string = environment.apiUrl;
  loading: boolean = false;
  constructor(
    public router: Router
  ) { }
    // show loading
    showLoader(): void {
      this.loading = true;
    }
    // hide loading
    hiderLoader(): void {
      this.loading = false;
    }
    // navigate to page
    navigateToUrl(url: string = ''): void {
      this.router.navigateByUrl(url);
    }
}

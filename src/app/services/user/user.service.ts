import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // api url
  apiUrl: string = environment.apiUrl;
  constructor( private http: HttpClient) { }
  // get all users
  getUsers(): Observable<any> {
    return ( this.http.get(`${this.apiUrl}/users`) );
  }
  // get user by name
  getUserByName(name: any): Observable<any>  {
    return ( this.http.get(`${this.apiUrl}/users/${name}`) );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserRoutingModule } from './user-routing.module';
import { UserCardDetailComponent } from 'src/app/components/user-card-detail/user-card-detail.component';
import { LoaderComponent } from 'src/app/components/loader/loader.component';
import { UserFormComponent } from 'src/app/components/forms/user/user-form.component';
import { AddUserComponent } from './add-user/add-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule
  ],
  declarations: [
    UserListComponent,
    UserDetailComponent,
    UserCardDetailComponent,
    LoaderComponent,
    AddUserComponent,
    UserFormComponent,
  ],
  entryComponents: []
})
export class UserModule { }

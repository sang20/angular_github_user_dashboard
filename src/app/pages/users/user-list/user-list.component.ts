import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { UtilityService } from 'src/app/services/utils/utils.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserListComponent implements OnInit {

  pageMeta = {
    title: 'Github User Profile Viewer',
    description: 'Search for a git user and hit enter.'
  };
  userInput: string;
  errorMsg: string;
  users: Array<any> = [];

  constructor(
    private userService: UserService,
    private utils: UtilityService) { }

  ngOnInit(): void {
    this.getAllUser();
  }
  // get all user
  getAllUser(): void {
    // show loader
    this.utils.showLoader();
    // fetch data to api
    this.userService.getUsers().subscribe(
      response => {
        // set data
        this.users = JSON.parse(JSON.stringify(response));
        // hide loader
        this.utils.hiderLoader();
      },
      error => {
        // error on api
        // set error message
        this.errorMsg = error.error.message;
        // hide loader
        this.utils.hiderLoader();
      }
    );
  }
  // search user by name
  searchUserByName(): void {
    // show loader
    this.utils.showLoader();
    // clear users array
    this.users = [];
    // clear error msg
    this.errorMsg = undefined;
    // fetch to api
    this.userService.getUserByName(this.userInput).subscribe(
      response => {
        // set data
        const data = JSON.parse(JSON.stringify(response));
        // push user data to users array
        this.users.push(data);
        // hide loader
        this.utils.hiderLoader();
      },
      error => {
        // error on api
        // set error message
        this.errorMsg = error.error.message;
        // hide loader
        this.utils.hiderLoader();
      }
    );
  }
  // navigate to detail page
  goToUserDetail(username: string = ''): void {
    this.utils.navigateToUrl(`user/${username}`);
  }
}

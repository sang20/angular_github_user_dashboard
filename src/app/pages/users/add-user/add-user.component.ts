import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddUserComponent implements OnInit {

  pageMeta = {
    title: 'Add Git User',
    description: 'This only a mock add user form that will not save any data to Github API. For UI and Form validation purpose only.'
  };

  constructor() {}

  ngOnInit() {
  }
}

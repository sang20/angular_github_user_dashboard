import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserData } from 'src/app/components/user-card-detail/user-card-detail.model';
import { UserService } from 'src/app/services/user/user.service';
import { UtilityService } from 'src/app/services/utils/utils.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserDetailComponent implements OnInit {

  pageMeta = {
    title: 'Git User Profile Details'
  };
  username: string;
  user: UserData;
  errorMsg: string;
  hasError: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private utils: UtilityService,
    private userService: UserService
  ) {
    // get username on url param
    this.username = this.route.snapshot.paramMap.get('username');
  }

  ngOnInit() {
    this.getUserByName(this.username);
  }
    // get user data by name
    getUserByName(username: string = ''): void {
      // show loader
      this.utils.showLoader();
      // fetch to api
      this.userService.getUserByName(username).subscribe(
        response => {
          // set data
          this.user = response;
          // hide loader
          this.utils.hiderLoader();
          this.hasError = false;
        },
        error => {
          // error on api
          // set error message
          this.errorMsg = error.error.message;
          // hide loader
          this.utils.hiderLoader();
          if (this.errorMsg !== undefined && !this.utils.loading) {
            this.hasError = true;
          }
        }
      );
    }
}
